import React, { Component } from "react";
import ForecastTitle from "./ForecastTitle";
import ForecastResult from "./ForecastResult";
import ForecastForm from "./ForecastForm";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fetchForecast } from "../actions/forecast";

class Forecast extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: "",
    };
  }
  onChange = (e) => {
    this.setState({ city: e.target.value });
  };

  render() {
    const { forecast } = this.props;
    const { city } = this.state;
    return (
      <div>
        {forecast.request ? (
          <>
            {" "}
            <ForecastTitle
              city={forecast.location.name}
              date={forecast.location.localtime}
              country={forecast.location.country}
            />
            <ForecastResult
              temperature={forecast.current.temperature}
              comment={forecast.current.weather_descriptions}
              img={forecast.current.weather_icons}
              wind={forecast.current.wind_speed}
              humidity={forecast.current.humidity}
            />
          </>
        ) : null}

        <ForecastForm
          onChange={this.onChange}
          onClick={() => {
            this.props.fetchForecast(city);
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    forecast: state.forecast,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      fetchForecast,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Forecast);
