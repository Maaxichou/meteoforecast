import React, { Component } from "react";

class ForecastForm extends Component {
  render() {
    return (
      <div className="searchBar">
        <button
          onClick={this.props.onClick}
          className="searchButton"
        ></button>
        <input
          name="citySearch"
          className="inputSearch"
          defaultValue=""
          type="text"
          onChange={this.props.onChange}
        />
      </div>
    );
  }
}

export default ForecastForm;
